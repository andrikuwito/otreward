import 'package:flutter/material.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:vinyard_point/page/mobileBody.dart';
import 'package:vinyard_point/preferences.dart';
import 'package:vinyard_point/signin/signin.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  // ignore: deprecated_member_use
  FlutterNativeSplash.removeAfter(initialization);
  runApp(MyApp());
}
Future initialization(BuildContext? context)async{
  await Future.delayed(Duration(milliseconds: 1));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyLoginPage(),
    );
  }
}

class MyLoginPage extends StatefulWidget {
  @override
  _MyLoginPageState createState() => _MyLoginPageState();
}

class _MyLoginPageState extends State<MyLoginPage> {
  bool? user;
  @override
  void initState() {
    super.initState();
    check_if_already_login();
  }

  void check_if_already_login() async {
    user = await getIsLogin();
    if (user != null && user == true) {
      Navigator.pushReplacement(
          context, new MaterialPageRoute(builder: (context) => MyMobile()));
    } else {
      Navigator.pushReplacement(
          context, new MaterialPageRoute(builder: (context) => SignIn()));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold();
    //   return Scaffold(
    //     appBar: AppBar(
    //       title: Text(" Shared Preferences"),
    //     ),
    //     body: Center(
    //       child: Column(
    //         crossAxisAlignment: CrossAxisAlignment.center,
    //         children: <Widget>[
    //           Text(
    //             "Login Form",
    //             style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
    //           ),
    //           Text(
    //             "To show Example of Shared Preferences",
    //             style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
    //           ),
    //           Padding(
    //             padding: const EdgeInsets.all(15.0),
    //             child: TextField(
    //               controller: username_controller,
    //               decoration: InputDecoration(
    //                 border: OutlineInputBorder(),
    //                 labelText: 'username',
    //               ),
    //             ),
    //           ),
    //           Padding(
    //             padding: const EdgeInsets.all(15.0),
    //             child: TextField(
    //               controller: password_controller,
    //               decoration: InputDecoration(
    //                 border: OutlineInputBorder(),
    //                 labelText: 'Password',
    //               ),
    //             ),
    //           ),
    //           RaisedButton(
    //             textColor: Colors.white,
    //             color: Colors.blue,
    //             onPressed: () {
    //               String username = username_controller.text;
    //               String password = password_controller.text;
    //               if (username != '' && password != '') {
    //                 print('Successfull');
    //                 logindata.setBool('login', false);
    //                 logindata.setString('username', username);
    //                 Navigator.of(context).pushAndRemoveUntil(
    //                     MaterialPageRoute(builder: (context) => MyMobile()),
    //                     (Route<dynamic> route) => false);
    //               }
    //             },
    //             child: Text("Log-In"),
    //           )
    //         ],
    //       ),
    //     ),
    //   );
    // }
  }
}
