import 'package:shared_preferences/shared_preferences.dart';

SharedPreferences? preferences;

Future<void> initializePreference(isLogin, String userEmail, String userPhone,
    String userName, String userAccount, String userCustomer) async {
  preferences = await SharedPreferences.getInstance();
  preferences?.setBool("isLogin", isLogin);
  preferences?.setString("Email", userEmail);
  preferences?.setString("Phone", userPhone);
  preferences?.setString("Name", userName);
  preferences?.setString("Account", userAccount);
  preferences?.setString("CustomerID", userCustomer);
}

getEmail() async {
  preferences = await SharedPreferences.getInstance();
  String? userEmail = preferences!.getString('Email');
  return userEmail;
}

getName() async {
  preferences = await SharedPreferences.getInstance();
  String? userName = preferences!.getString('Name');
  return userName;
}

getAccount() async {
  preferences = await SharedPreferences.getInstance();
  String? userAccount = preferences!.getString('Account');
  return userAccount;
}

getCustomerID() async {
  preferences = await SharedPreferences.getInstance();
  String? userCustomerID = preferences!.getString('CustomerID');
  return userCustomerID;
}

getPhone() async {
  preferences = await SharedPreferences.getInstance();
  String? userPhone = preferences!.getString('Phone');
  return userPhone;
}

Future<bool> getIsLogin() async {
  preferences = await SharedPreferences.getInstance();
  bool? boolValue = preferences!.getBool('isLogin');
  bool? checkValue = preferences!.containsKey('isLogin');
  if (checkValue && boolValue != null) {
    return true;
  }
  return false;
}

clearSharedPreferences() async{
  preferences = await SharedPreferences.getInstance();
  preferences!.clear();
}