import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:http/http.dart' as http;
import 'package:vinyard_point/preferences.dart';

class History extends StatefulWidget {
  const History({Key? key}) : super(key: key);
  @override
  State<History> createState() => _HistoryState();
}

List point = [];

class _HistoryState extends State<History> {
  String account = "";

  @override
  void initState() {
    super.initState();
    getData();
  }

  Future<dynamic> getData() async {
    account = await getAccount();
    setState(() {
      account;
    });
  }

  Future<String> getHistory() async {
    try {
      var response = await http.get(Uri.parse(
          "http://116.213.55.69/LSPoin/api/LSPoin/GetHistory?Account=$account"));
      if (response.statusCode == 200) {
        point.clear();
        var panjang = json.decode(response.body)['ReturnObject'].length;
        for (int i = 0; i < panjang; i++) {
          var x = {
            "Amount": json.decode(response.body)['ReturnObject'][i]['Amount'],
            "TransDate": json.decode(response.body)['ReturnObject'][i]
                ['TransDate'],
            "TransType": json.decode(response.body)['ReturnObject'][i]
                ['TransType']
          };
          point.add(x);
        }
        if (point.isNotEmpty) {
          return "ada";
        } else {
          return "kosong";
        }
      } else {
        return "kosong";
      }
    } catch (e) {
      print(e.toString());
      return "kosong";
    }
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      getHistory();
    });
    return Scaffold(
      appBar: AppBar(
        title: Text("Riwayat Transaksi"),
        backgroundColor: Color.fromRGBO(13, 125, 160, 1),
      ),
      body: point.isEmpty
          ? emptyBody()
          : ListView.builder(
              itemCount: point.length,
              itemBuilder: (BuildContext context, int index) {
                return SizedBox(
                    width: double.infinity,
                    height: 100,
                    child: Card(
                      child: Container(
                        padding: EdgeInsets.all(20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(point[index]["TransType"]),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(point[index]["TransDate"]),
                                Text(point[index]["Amount"].toString())
                              ],
                            )
                          ],
                        ),
                      ),
                    ));
              }),
    );
  }

  Widget emptyBody() {
    return Center(
      child: Column(children: [
        Container(
          padding: EdgeInsets.all(20),
          height: 250,
          child: Image.asset('assets/images/history.jpeg'),
        ),
        Text(
          "Tidak Ada Transaksi",
          style:
              TextStyle(color: Color.fromRGBO(13, 125, 160, 1), fontSize: 20),
        ),
        Text("Kami tidak melihat catatan apa pun dari riwayat Anda")
      ]),
    );
  }
}
