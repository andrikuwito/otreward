import 'dart:convert';

import 'package:another_flushbar/flushbar.dart';
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:http/http.dart' as http;
import 'package:vinyard_point/preferences.dart';

class ChangePassword extends StatefulWidget {
  const ChangePassword({Key? key}) : super(key: key);
  @override
  State<ChangePassword> createState() => _ChangePasswordState();
}

var oldPasswordController = TextEditingController();
var newPasswordController = TextEditingController();
var confirmPasswordController = TextEditingController();

class _ChangePasswordState extends State<ChangePassword> {
  String customerID = "";
  @override
  void initState() {
    super.initState();
    getData();
  }

  Future<dynamic> getData() async {
    customerID = await getCustomerID();
    setState(() {
      customerID;
    });
  }

  Future<String> forgotpassword(
      String oldPassword, String newPassword, String confirmPassword) async {
    var oldpass = utf8.encode(oldPassword);
    String oldPassConvert = sha1.convert(oldpass).toString();

    var newpass = utf8.encode(newPassword);
    String newPassConvert = sha1.convert(newpass).toString();

    var confirmpass = utf8.encode(confirmPassword);
    String confirmPassConvert = sha1.convert(confirmpass).toString();

    print(oldPassConvert);
    print(newPassConvert);

    if (newPassConvert == confirmPassConvert) {
      var response = await http.post(
          Uri.parse("http://116.213.55.69/LSPoin/api/LSPoin/ChangePassword"),
          headers: {"Content-Type": "application/json; charset=utf-8"},
          body: json.encode({
            "CustomerID": customerID,
            "OldPass": oldPassConvert,
            "NewPass": newPassConvert
          }));

      if (json.decode(response.body)['StatusCode'] == 1) {
        print(json.decode(response.body)['ReturnObject']["Message"]);
        return json.decode(response.body)['ReturnObject']["Message"];
      } else {
        print(json.decode(response.body)['ReturnObject']["Message"]);
        return "Password salah";
      }
    } else {
      print("pass kaga sama");
      return "gagal";
    }
  }

  void clearText() {
    oldPasswordController.clear();
    newPasswordController.clear();
    confirmPasswordController.clear();
  }

  @override
  Widget build(BuildContext context) {
    print("aku");
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
          leading: CloseButton(
            color: Colors.black,
          ),
          elevation: 0,
          backgroundColor: Colors.transparent),
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage("assets/images/background.jpeg"),
          fit: BoxFit.cover,
        )),
        child: Container(
          padding: EdgeInsets.all(30),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    Text("UBAH KATA SANDI",
                        style: TextStyle(
                          fontSize: 18,
                          fontFamily: 'opensansExtraBold',
                        )),
                    Container(
                      padding: EdgeInsets.only(bottom: 30),
                    ),
                    Form(
                        child: Column(
                      children: [
                        TextField(
                          decoration: InputDecoration(
                              labelText: 'Kata Sandi Lama',
                              labelStyle: TextStyle(
                                  color: Color.fromRGBO(6, 126, 147, 1)),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25),
                                borderSide: BorderSide(
                                    width: 2,
                                    color: Color.fromRGBO(
                                        6, 126, 147, 1)), //<-- SEE HERE
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: const BorderSide(
                                    width: 2,
                                    color: Color.fromRGBO(6, 126, 147, 1)),
                                borderRadius: BorderRadius.circular(25),
                              )),
                          controller: oldPasswordController,
                          obscureText: true,
                          enableSuggestions: false,
                          autocorrect: false,
                        ),
                        Container(
                          padding: EdgeInsets.only(bottom: 30),
                        ),
                        TextField(
                          decoration: InputDecoration(
                              labelText: 'Kata Sandi Baru',
                              labelStyle: TextStyle(
                                  color: Color.fromRGBO(6, 126, 147, 1)),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25),
                                borderSide: BorderSide(
                                    width: 2,
                                    color: Color.fromRGBO(
                                        6, 126, 147, 1)), //<-- SEE HERE
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: const BorderSide(
                                    width: 2,
                                    color: Color.fromRGBO(6, 126, 147, 1)),
                                borderRadius: BorderRadius.circular(25),
                              )),
                          controller: newPasswordController,
                          obscureText: true,
                          enableSuggestions: false,
                          autocorrect: false,
                        ),
                        Container(
                          padding: EdgeInsets.only(bottom: 30),
                        ),
                        TextField(
                          decoration: InputDecoration(
                              labelText: 'Konfirmasi Kata Sandi',
                              labelStyle: TextStyle(
                                  color: Color.fromRGBO(6, 126, 147, 1)),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25),
                                borderSide: BorderSide(
                                    width: 2,
                                    color: Color.fromRGBO(6, 126, 147, 1)),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: const BorderSide(
                                    width: 2,
                                    color: Color.fromRGBO(6, 126, 147, 1)),
                                borderRadius: BorderRadius.circular(25),
                              )),
                          controller: confirmPasswordController,
                          obscureText: true,
                          enableSuggestions: false,
                          autocorrect: false,
                        ),
                        Container(
                          padding: EdgeInsets.only(bottom: 30),
                        ),
                      ],
                    )),
                    Text(
                      "Kata sandi yang kuat memiliki kombinasi huruf dan angka dan karakter khusus seperti \$,!,%,dll",
                      style: TextStyle(
                        fontFamily: 'opensansSemiBold',
                      ),
                    )
                  ],
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.8,
                  height: MediaQuery.of(context).size.height * 0.065,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(30.0),
                        ),
                        padding: EdgeInsets.zero,
                        primary: Color.fromRGBO(6, 126, 147, 1),
                        shadowColor: Colors.transparent,
                        elevation: 0.0),
                    onPressed: () {
                      if (oldPasswordController.text == "") {
                        Flushbar(
                          flushbarPosition: FlushbarPosition.TOP,
                          backgroundColor: Colors.red,
                          message: "Kata sandi lama tidak boleh kosong",
                          duration: Duration(seconds: 3),
                        )..show(context);
                      } else {
                        if (newPasswordController.text == "") {
                          Flushbar(
                            flushbarPosition: FlushbarPosition.TOP,
                            backgroundColor: Colors.red,
                            message: "Kata sandi baru tidak boleh kosong",
                            duration: Duration(seconds: 3),
                          )..show(context);
                        } else {
                          if (confirmPasswordController.text == "") {
                            Flushbar(
                              flushbarPosition: FlushbarPosition.TOP,
                              backgroundColor: Colors.red,
                              message:
                                  "Kata sandi konfirmasi tidak boleh kosong",
                              duration: Duration(seconds: 3),
                            )..show(context);
                          } else {
                            forgotpassword(
                                    oldPasswordController.text,
                                    newPasswordController.text,
                                    confirmPasswordController.text)
                                .then((value) {
                              if (value == "Berhasil mengganti password") {
                                Flushbar(
                                  flushbarPosition: FlushbarPosition.TOP,
                                  backgroundColor: Colors.green,
                                  message: "Berhasil Ganti Password",
                                  duration: Duration(seconds: 3),
                                )..show(context);
                                return "Berhasil Ganti Password";
                              } else if (value == "Password salah") {
                                Flushbar(
                                  flushbarPosition: FlushbarPosition.TOP,
                                  backgroundColor: Colors.red,
                                  message: "Password Salah",
                                  duration: Duration(seconds: 3),
                                )..show(context);
                              } else if (value == "gagal") {
                                Flushbar(
                                  flushbarPosition: FlushbarPosition.TOP,
                                  backgroundColor: Colors.red,
                                  message: "Password Tidak Sama",
                                  duration: Duration(seconds: 3),
                                )..show(context);
                              }
                              clearText();
                            });
                          }
                        }
                      }
                    },
                    child: Text("SIMPAN"),
                  ),
                ),
              ]),
        ),
      ),
    );
  }
}
