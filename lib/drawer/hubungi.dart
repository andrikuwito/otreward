import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class ContactUs extends StatefulWidget {
  const ContactUs({Key? key}) : super(key: key);

  @override
  State<ContactUs> createState() => _ContactUsState();
}

class _ContactUsState extends State<ContactUs> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text("Customer Care"),
        backgroundColor: Color.fromRGBO(6, 126, 147, 1),
      ),
      body: Container(
        padding: EdgeInsets.all(30),
        child: SizedBox(
          height: height * 0.15,
          width: width,
          child: Container(
            clipBehavior: Clip.hardEdge,
            decoration: BoxDecoration(
                border: Border.all(color: Color.fromRGBO(6, 126, 147, 1)),
                borderRadius: BorderRadius.circular(10)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    color: Color.fromRGBO(6, 126, 147, 1),
                    height: height * 0.05,
                    width: width,
                    child: Center(
                      child: Text(
                        "CUSTOMER CARE",
                        style: TextStyle(
                          fontSize: 22,
                          color: Colors.white,
                          fontFamily: 'opensansBold',
                        ),
                      ),
                    )),
                Container(
                  padding: EdgeInsets.only(top: 10, left: 10),
                  child: Text(
                    "0811-1067-744",
                    style: TextStyle(
                      fontSize: 18,
                      color: Color.fromRGBO(6, 126, 147, 1),
                      fontFamily: 'opensansBold',
                    ),
                  ),
                ),

                Container(
                  padding: EdgeInsets.only(top: 5, left: 10),
                  child: Text(
                    "WA/CALL",
                    style: TextStyle(
                      fontSize: 12,
                      color: Color.fromRGBO(6, 126, 147, 1),
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  child: Divider(
                    thickness: 1,
                    color: Color.fromRGBO(6, 126, 147, 1),
                  ),
                )
                //     Row(
                // mainAxisAlignment: MainAxisAlignment.end,
                // children: [
                //   Container(
                //     padding: EdgeInsets.only(top: 20,right: 20),
                //     child: Icon(
                //       Icons.phone_in_talk_rounded,
                //       size: 30,
                //     ),
                //   )
                // ],
                //     )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
