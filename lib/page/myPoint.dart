import 'dart:convert';

import 'package:barcode/barcode.dart';
import 'package:barcode_widget/barcode_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vinyard_point/preferences.dart';
import 'package:http/http.dart' as http;

class MyPoint extends StatefulWidget {
  const MyPoint({Key? key}) : super(key: key);

  @override
  State<MyPoint> createState() => _MyPointState();
}

class _MyPointState extends State<MyPoint> {
  String accountNo = "";
  String customerName = "";

  void initState() {
    super.initState();
    getData();
  }

  Future<dynamic> getData() async {
    accountNo = await getAccount();
    customerName = await getName();
    setState(() {
      customerName;
      accountNo;
    });
  }

  int point = 0;

  Future<void> getpoint() async {
    try {
      var response = await http.get(Uri.parse(
          "http://116.213.55.69/LSPoin/api/LSPoin/GetPoint?Account=$accountNo"));
      if (response.statusCode == 200) {
        var isiPoint = json.decode(response.body)['ReturnObject']['Point'];
        print(isiPoint);
        point = isiPoint.toInt();
      }
    } catch (e) {
      print(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          width: double.infinity,
          decoration: BoxDecoration(
              image: DecorationImage(
            image: AssetImage("assets/images/background.jpeg"),
            fit: BoxFit.cover,
          )),
          child: getPointBody()),
    );
  }

  void reload() {
    setState(() {
      getpoint();
      print("reload");
    });
  }

  Widget getPointBody() {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Center(
      child: Column(
        children: [
          Container(
              padding: EdgeInsets.only(top: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 50),
                  ),
                  Text(
                    customerName,
                    style: TextStyle(
                      fontSize: 20,
                      fontFamily: 'opensansExtraBold',
                    ),
                  ),
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: Colors.transparent,
                          shadowColor: Colors.transparent,
                          elevation: 0,
                          padding: EdgeInsets.zero),
                      onPressed: (){reload();},
                      child: Icon(
                        Icons.replay_circle_filled,
                        color: Colors.amber,
                        size: 35,
                      )),
                ],
              )),
          Container(
            padding: EdgeInsets.only(top: 90),
            child: Text("Point anda:",
                style: TextStyle(
                  fontSize: 20,
                  fontFamily: 'opensansBold',
                )),
          ),
          Container(
            padding: EdgeInsets.only(top: 10),
            child: Text(point.toString(),
                style: TextStyle(
                    fontSize: 20,
                    fontFamily: 'opensansBold',
                    color: Color.fromRGBO(6, 126, 147, 1))),
          ),
          ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary: Colors.transparent, elevation: 0.0),
              onPressed: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => pop()));
              },
              child: SizedBox(
                height: height * 0.12,
                width: width,
                child: Card(
                  shape: RoundedRectangleBorder(
                      side: BorderSide(
                        color: Colors.black,
                      ),
                      borderRadius: BorderRadius.circular(20.0)),
                  child: Column(
                    children: [
                      Container(
                          padding:
                              EdgeInsets.only(top: 20, left: 20, right: 20),
                          width: width,
                          height: 80,
                          child: BarcodeWidget(
                            barcode: Barcode.code128(),
                            data: accountNo,
                          )),
                    ],
                  ),
                ),
              ))
        ],
      ),
    );
  }

  Widget pop() {
    return Scaffold(
      appBar: AppBar(
          title: Text(accountNo),
          backgroundColor: Color.fromRGBO(13, 125, 160, 1)),
      body: Center(
        child: Container(
          padding: EdgeInsets.all(20),
          height: 200,
          child: BarcodeWidget(barcode: Barcode.code128(), data: accountNo),
        ),
      ),
    );
  }
}
