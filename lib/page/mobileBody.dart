import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:timelines/timelines.dart';
import 'package:vinyard_point/drawer/changePassword.dart';
import 'package:vinyard_point/drawer/hubungi.dart';
import 'package:vinyard_point/maps/maps.dart';
import 'package:vinyard_point/page/myPoint.dart';
import 'package:vinyard_point/drawer/transaksi.dart';
import 'package:vinyard_point/preferences.dart';
import 'package:vinyard_point/signin/signin.dart';

class MyMobile extends StatefulWidget {
  const MyMobile({Key? key}) : super(key: key);

  @override
  State<MyMobile> createState() => _MyMobileState();
}

List promo = [];
List catalog = [];

class _MyMobileState extends State<MyMobile> {
  String email = "";

  int _selectedIndex = 0;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  Future<void> getpromo() async {
    try {
      var response = await http.get(Uri.parse(
          "http://116.213.55.69/LSPoin/api/LSPoin/GetPromoSlider?Company=VY"));
      if (response.statusCode == 200) {
        var isi_promo = json.decode(response.body)['ReturnObject'];
        promo.clear();
        if (isi_promo.length != 0) {
          for (int i = 0;
              i <= json.decode(response.body)['ReturnObject'].length;
              i++) {
            promo.add(json.decode(response.body)['ReturnObject'][i]);
            print("http://" + promo[0]["SliderImage"]);
          }
        }
      }
    } catch (e) {
      print(e.toString());
    }
  }

  Future<void> getcatalog() async {
    try {
      var response = await http.get(Uri.parse(
          "http://116.213.55.69/LSPoin/api/LSPoin/GetCatalog?Company=VY"));
      if (response.statusCode == 200) {
        var isiCatalog = json.decode(response.body)['ReturnObject'];
        catalog.clear();
        if (isiCatalog.length != 0) {
          var panjang = isiCatalog.length;
          for (int i = 0; i <= panjang; i++) {
            catalog.add(json.decode(response.body)['ReturnObject'][i]);
          }
        }
      }
    } catch (e) {
      print(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    getpromo();
    getcatalog();
    setState(() {
      promo;
      catalog;
    });
  }

  @override
  Widget build(BuildContext context) {
    final List<Widget> page = [getMobileBody(), MyPoint()];
    return Scaffold(
        drawer: NavigationDrawer(),
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: Icon(Icons.home),
                label: 'Home',
                backgroundColor: Color.fromRGBO(6, 126, 147, 1)),
            BottomNavigationBarItem(
                icon: Icon(Icons.credit_card),
                label: 'My Point',
                backgroundColor: Color.fromRGBO(6, 126, 147, 1)),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: Color.fromRGBO(6, 126, 147, 1),
          onTap: _onItemTapped,
        ),
        appBar: AppBar(
          centerTitle: true,
          title: Image.asset('assets/images/logo2.png', fit: BoxFit.cover),
          backgroundColor: Color.fromRGBO(6, 126, 147, 1),
        ),
        body: Container(
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/background.jpeg"),
                fit: BoxFit.cover,
              ),
            ),
            child: IndexedStack(index: _selectedIndex, children: page)));
  }
}

class getMobileBody extends StatefulWidget {
  const getMobileBody({Key? key}) : super(key: key);

  @override
  State<getMobileBody> createState() => _getMobileBodyState();
}

class _getMobileBodyState extends State<getMobileBody> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.only(left: 20, right: 20, top: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Good Afternoon",style: TextStyle(fontSize: 25),),
            SizedBox(height: 10,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Andri Kuwito",style: TextStyle(fontSize: 20),),
                Text("Silver Level",style: TextStyle(fontSize: 20),),
            ],),
            SizedBox(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height*0.16,
              child: Card(
                color: Colors.grey,
                child: Column(
                  children: [
                    Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Text("125",style: TextStyle(fontSize: 25),),
                                Icon(Icons.star,size: 30,),
                              ],
                            ),
                            GestureDetector(
                              child: Text("Detail",style: TextStyle(fontSize: 20)),
                            ),
                          ],
                        ),
                        Center(
                          child: SizedBox(
                            height: MediaQuery.of(context).size.height*0.08,
                            child: Timeline.tileBuilder(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              scrollDirection: Axis.horizontal,
                              builder: TimelineTileBuilder.fromStyle(
                                contentsAlign: ContentsAlign.basic,
                                contentsBuilder: (context, index) => Padding(
                                  padding: const EdgeInsets.all(1),
                                  child: Text('$index'),
                                ),
                                itemExtent: 55.0,
                                itemCount: 6,
                              ),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
            SizedBox(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height*0.3,
            child: Card(
              color: Colors.amber,
              child: Container(
                padding: EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 100,
                      width: MediaQuery.of(context).size.width, 
                      child: Card(
                        child: Image.network("https://instagram.fcgk10-1.fna.fbcdn.net/v/t51.2885-15/345642505_751332339874836_3173533131350578190_n.webp?stp=dst-jpg_e35&_nc_ht=instagram.fcgk10-1.fna.fbcdn.net&_nc_cat=100&_nc_ohc=twd2TiWRzZoAX-1ln9R&edm=ACWDqb8BAAAA&ccb=7-5&ig_cache_key=MzA5NzgzNTM1MTE2MTc2NTU4MQ%3D%3D.2-ccb7-5&oh=00_AfBkZqOS_dwX-_nFCrWTKUEkxOkcp8KrpiFNBtZXUr6w_A&oe=645D46A9&_nc_sid=1527a3",scale: 1,),
                      ),
                    ),
                    Text("Promo Minuman",style: TextStyle(fontSize: 18),),
                    SizedBox(height: 10,),
                    Text("Minuman manis yang praktis dibawa kemana-mana ini, bisa kamu dapatkan dengan harga promo loh!"),
                    ElevatedButton(onPressed: (){}, child: Text("Detail"))
                  ]
                ),
              ),
            ),
          ),
          ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: 2,
            itemBuilder: (BuildContext context, int index) {
            return SizedBox(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height*0.2,
              child: Card(
                child: Container(
                  padding: EdgeInsets.all(10),
                  child: Row(
                    children: [
                      Image.network("https://instagram.fcgk10-1.fna.fbcdn.net/v/t51.2885-15/345642505_751332339874836_3173533131350578190_n.webp?stp=dst-jpg_e35&_nc_ht=instagram.fcgk10-1.fna.fbcdn.net&_nc_cat=100&_nc_ohc=twd2TiWRzZoAX-1ln9R&edm=ACWDqb8BAAAA&ccb=7-5&ig_cache_key=MzA5NzgzNTM1MTE2MTc2NTU4MQ%3D%3D.2-ccb7-5&oh=00_AfBkZqOS_dwX-_nFCrWTKUEkxOkcp8KrpiFNBtZXUr6w_A&oe=645D46A9&_nc_sid=1527a3",scale: 1,),
                      SizedBox(width: 10,),
                      SizedBox(
                        width: MediaQuery.of(context).size.width*0.4,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Judul"),
                            SizedBox(height: 10,),
                            Text("Minuman manis yang praktis dibawa kemana-mana ini, bisa kamu dapatkan dengan harga promo loh!"),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          })
        ]),
      ),
    );
  }
}

class NavigationDrawer extends StatefulWidget {
  const NavigationDrawer({Key? key, this.userNav}) : super(key: key);
  final userNav;
  @override
  State<NavigationDrawer> createState() => _NavigationDrawerState();
}

class _NavigationDrawerState extends State<NavigationDrawer> {
  String email = "";
  String telp = "";
  String nama = "";
  @override
  void initState() {
    super.initState();
    refreshHome();
  }

  Future<void> refreshHome() async {
    await getData();
    return;
  }

  Future<dynamic> getData() async {
    initState() {}
    email = await getEmail();
    telp = await getPhone();
    nama = await getName();
    setState(() {
      email;
      telp;
      nama;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: Column(
      children: [
        Expanded(
          child: ListView(
            padding: EdgeInsets.zero,
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.19,
                child: DrawerHeader(
                  decoration:
                      BoxDecoration(color: Color.fromRGBO(6, 126, 147, 1)),
                  child: FittedBox(
                    child: Container(
                      padding: EdgeInsets.only(right: 50, bottom: 5, top: 10),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              nama,
                              style: TextStyle(
                                fontSize: 10,
                                color: Colors.white,
                                fontFamily: 'opensansExtraBold',
                              ),
                            ),
                            Text(
                              email,
                              style:
                                  TextStyle(fontSize: 8, color: Colors.white),
                            ),
                            Text(
                              telp,
                              style:
                                  TextStyle(fontSize: 8, color: Colors.white),
                            )
                          ]),
                    ),
                  ),
                ),
              ),
              drawerItem(
                  icon: Icons.history,
                  text: "History Transaksi",
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => History()));
                  }),
              drawerItem(
                  icon: Icons.store,
                  text: "Lokasi Toko",
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Maps()));
                  }),
              drawerItem(
                  icon: Icons.lock,
                  text: "Ubah Kata Sandi",
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ChangePassword()));
                  }),
              drawerItem(
                  icon: Icons.call,
                  text: "Hubungi Kami",
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => ContactUs()));
                  }),
              drawerItem(
                  icon: Icons.exit_to_app,
                  text: "Keluar",
                  onTap: () => showDialog<String>(
                        context: context,
                        builder: (BuildContext context) => AlertDialog(
                          title: const Text('Keluar'),
                          actions: <Widget>[
                            Container(
                              padding: EdgeInsets.only(left: 18),
                              child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text("Anda Yakin Ingin Keluar?")),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                TextButton(
                                  onPressed: () => Navigator.pop(context),
                                  child: const Text('BATAL'),
                                ),
                                TextButton(
                                  onPressed: () {
                                    clearSharedPreferences();
                                    Navigator.of(context).pushAndRemoveUntil(
                                        MaterialPageRoute(
                                            builder: (context) => SignIn()),
                                        (Route<dynamic> route) => false);
                                  },
                                  child: const Text('YA'),
                                ),
                              ],
                            )
                          ],
                        ),
                      )),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.only(bottom: 10),
          child: Align(
            alignment: Alignment.bottomCenter,
            child: Text(
              "v1.01.05",
              style: TextStyle(
                fontFamily: 'opensansExtraBold',
              ),
            ),
          ),
        )
      ],
    ));
  }

  Widget drawerItem(
      {required IconData icon,
      required String text,
      required GestureTapCallback onTap}) {
    return ListTile(
        title: Row(
          children: [
            Icon(icon),
            Container(
              padding: EdgeInsets.only(left: 20),
            ),
            Text(text)
          ],
        ),
        onTap: onTap);
  }
}
