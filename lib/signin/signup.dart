import 'dart:convert';

import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:http/http.dart' as http;
import 'package:vinyard_point/signin/ketentuan.dart';
import 'package:vinyard_point/signin/signin.dart';
import 'package:url_launcher/url_launcher.dart';

class SignUp extends StatefulWidget {
  SignUp({Key? key}) : super(key: key);

  @override
  State<SignUp> createState() => _SignUpState();
}

List user = [];
Future<String> register() async {
  var response = await http.post(
      Uri.parse("http://116.213.55.69/LSPoin/api/LSPoin/SignUp"),
      headers: {"Content-Type": "application/json; charset=utf-8"},
      body: json.encode({
        "Name": fullname.text,
        "Email": email.text,
        "Phone": telp.text,
        "Company": "VY"
      }));
  if (response.statusCode == 200) {
    if (json.decode(response.body)['StatusCode'] == 1) {
      return "Password signup telah dikirim";
    } else {
      if (json.decode(response.body)['ReturnObject']["Message"] ==
          "Pendaftaran gagal, account dengan nomor hp atau email ini sudah ada") {
        return "Pendaftaran gagal, account dengan nomor hp atau email ini sudah ada";
      } else {
        return "gagal";
      }
    }
  } else {
    return "gagal";
  }
}

var fullname = TextEditingController();
var telp = TextEditingController();
var email = TextEditingController();

class _SignUpState extends State<SignUp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        leading: CloseButton(
          color: Colors.black,
        ),
        elevation: 0,
        backgroundColor: Colors.transparent,
      ),
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage("assets/images/background.jpeg"),
          fit: BoxFit.cover,
        )),
        padding: EdgeInsets.only(top: 30, left: 50, right: 50, bottom: 40),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Form(
                child: Column(
              children: [
                Text(
                  "BUAT AKUN BARU",
                  style: TextStyle(
                      fontFamily: 'opensansExtraBold',
                      fontSize: 18,
                      fontWeight: FontWeight.bold),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 30),
                ),
                TextField(
                  decoration: InputDecoration(
                      labelText: 'Nama Lengkap',
                      labelStyle:
                          TextStyle(color: Color.fromRGBO(6, 126, 147, 1)),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(25),
                        borderSide: BorderSide(
                            width: 2,
                            color:
                                Color.fromRGBO(6, 126, 147, 1)), //<-- SEE HERE
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: const BorderSide(
                            width: 2, color: Color.fromRGBO(6, 126, 147, 1)),
                        borderRadius: BorderRadius.circular(25),
                      )),
                  controller: fullname,
                ),
                Container(
                  padding: EdgeInsets.only(top: 20),
                ),
                TextField(
                    decoration: InputDecoration(
                        labelText: 'NO. Handphone',
                        labelStyle:
                            TextStyle(color: Color.fromRGBO(6, 126, 147, 1)),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25),
                          borderSide: BorderSide(
                              width: 2,
                              color: Color.fromRGBO(
                                  6, 126, 147, 1)), //<-- SEE HERE
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: const BorderSide(
                              width: 2, color: Color.fromRGBO(6, 126, 147, 1)),
                          borderRadius: BorderRadius.circular(25),
                        )),
                    controller: telp),
                Container(
                  padding: EdgeInsets.only(top: 20),
                ),
                TextField(
                    decoration: InputDecoration(
                        labelText: 'Email',
                        labelStyle:
                            TextStyle(color: Color.fromRGBO(6, 126, 147, 1)),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25),
                          borderSide: BorderSide(
                              width: 2,
                              color: Color.fromRGBO(
                                  6, 126, 147, 1)), //<-- SEE HERE
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: const BorderSide(
                              width: 2, color: Color.fromRGBO(6, 126, 147, 1)),
                          borderRadius: BorderRadius.circular(25),
                        )),
                    controller: email),
                Container(
                  padding: EdgeInsets.only(top: 20),
                ),
                // Text(
                //   "Dengan mengetuk 'Buat Akun Baru' anda menyetujui",
                //   textAlign: TextAlign.center,
                //   style: TextStyle(
                //     fontSize: 15,
                //     fontFamily: 'opensansSemiBold',
                //   ),
                // ),
                RichText(
                  textAlign: TextAlign.center,
                    text: new TextSpan(
                  children: [
                    new TextSpan(
                      text: "Dengan mengetuk 'Buat Akun Baru' anda menyetujui ",
                      style: TextStyle(
                        fontSize: 15,
                        fontFamily: 'opensansSemiBold',
                        color: Colors.black
                      ),
                    ),
                    new TextSpan(
                      text: 'persyaratan & ketentuan',
                      style: new TextStyle(fontSize: 15,
                        fontFamily: 'opensansSemiBold',color: Colors.red),
                      recognizer: new TapGestureRecognizer()
                        ..onTap = () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Ketentuan()));
                        },
                    ),
                  ],
                ))
              ],
            )),
            Container(
              width: MediaQuery.of(context).size.width * 0.8,
              height: MediaQuery.of(context).size.height * 0.065,
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0),
                      ),
                      padding: EdgeInsets.zero,
                      primary: Color.fromRGBO(6, 126, 147, 1),
                      shadowColor: Colors.transparent,
                      elevation: 0.0),
                  onPressed: () async {
                    if (fullname.text == "") {
                      Flushbar(
                        flushbarPosition: FlushbarPosition.TOP,
                        backgroundColor: Colors.red,
                        message: "Nama lengkap tidak boleh kosong",
                        duration: Duration(seconds: 3),
                      )..show(context);
                    } else if (telp.text == "") {
                      Flushbar(
                        flushbarPosition: FlushbarPosition.TOP,
                        backgroundColor: Colors.red,
                        message: "No.Handphone tidak boleh kosong",
                        duration: Duration(seconds: 3),
                      )..show(context);
                    } else if (email.text == "") {
                      Flushbar(
                        flushbarPosition: FlushbarPosition.TOP,
                        backgroundColor: Colors.red,
                        message: "Alamat email tidak boleh kosong",
                        duration: Duration(seconds: 3),
                      )..show(context);
                    } else {
                      bool emailValid = RegExp(
                              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                          .hasMatch(email.text);
                      if (emailValid == true) {
                        showDialog(
                            barrierDismissible: false,
                            context: context,
                            builder: (_) {
                              return Center(
                                child: SizedBox(
                                    width: 50,
                                    height: 50,
                                    child: CircularProgressIndicator()),
                              );
                            });
                        await register().then((value) {
                          Navigator.of(context).pop();
                          if (value == "Password signup telah dikirim") {
                            Flushbar(
                              flushbarPosition: FlushbarPosition.TOP,
                              backgroundColor: Colors.green,
                              message: "Password signup telah dikirim",
                              duration: Duration(seconds: 3),
                            )..show(context);
                            Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(
                                    builder: (context) => SignIn()),
                                (Route<dynamic> route) => false);
                          } else if (value ==
                              "Pendaftaran gagal, account dengan nomor hp atau email ini sudah ada") {
                            Flushbar(
                              flushbarPosition: FlushbarPosition.TOP,
                              backgroundColor: Colors.red,
                              message:
                                  "Pendaftaran gagal, account dengan nomor hp atau email ini sudah ada",
                              duration: Duration(seconds: 3),
                            )..show(context);
                          } else {
                            Flushbar(
                              flushbarPosition: FlushbarPosition.TOP,
                              backgroundColor: Colors.red,
                              message: "Gagal",
                              duration: Duration(seconds: 3),
                            )..show(context);
                          }
                        });
                      } else {
                        Flushbar(
                          flushbarPosition: FlushbarPosition.TOP,
                          backgroundColor: Colors.red,
                          message: "Email tidak valid",
                          duration: Duration(seconds: 3),
                        )..show(context);
                      }
                    }
                  },
                  child: Text("DAFTAR AKUN BARU")),
            )
          ],
        ),
      ),
    );
  }
}
