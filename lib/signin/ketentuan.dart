import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class Ketentuan extends StatefulWidget {
  const Ketentuan({Key? key}) : super(key: key);

  @override
  State<Ketentuan> createState() => _KetentuanState();
}

class _KetentuanState extends State<Ketentuan> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
            child: Text(
          "Syarat & Ketentuan",
          style: TextStyle(
            color: Color.fromRGBO(6, 126, 147, 1),
            fontFamily: 'opensansExtraBold',
          ),
        )),
        leading: CloseButton(
          color: Colors.black,
        ),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage("assets/images/background.jpeg"),
          fit: BoxFit.cover,
        )),
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    padding: EdgeInsets.only(bottom: 20),
                    child: Text(
                      "Program Vinyard Point",
                      style: TextStyle(
                          color: Color.fromRGBO(6, 126, 147, 1),
                          fontFamily: 'opensansBold',
                          fontSize: 15),
                    )),
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  child: Text(
                    "1. Program Vinyard Point adalah program loyalitas pelanggan yang ditawarakan oleh OT RETAIL kepada pelanggannya.",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(6, 126, 147, 1),
                        fontFamily: 'opensansSemiBold',
                        fontSize: 15),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  child: Text(
                    "2. Membership (Keanggotaan) dalam program ini gratis. Pendaftaran keanggotaan dapat dilakukan dengan mengunduk Aplikasi mobile Vinyard Point di Google Play Store.",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(6, 126, 147, 1),
                        fontFamily: 'opensansSemiBold',
                        fontSize: 15),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  child: Text(
                    "3. Syarat & Ketentuan ini merupakan perjanjian antara Member dan Vinyard Poiint sehubungan dengan Program. Dengan Berpartisipasi dalam Program, Member menyetujui Syarat & Ketentuan ini.",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(6, 126, 147, 1),
                        fontFamily: 'opensansSemiBold',
                        fontSize: 15),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  child: Text(
                    "4. Vinyard Point berhak untuk mengubah Syarat & Ketentuan setiap saat dengan alasan apa pun tanpa pemberitahuan tertulis sebelumnya. Member bertanggung jawab untuk melacak setiap pembaruan pada Syarat & Ketentuan .",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(6, 126, 147, 1),
                        fontFamily: 'opensansSemiBold',
                        fontSize: 15),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  child: Text(
                    "5. Vinyard Point berhak untuk menghentikan Program kapan saja dengan alasan apa pun.",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(6, 126, 147, 1),
                        fontFamily: 'opensansSemiBold',
                        fontSize: 15),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  child: Text(
                    "6. Membership hanya tersedia untuk individu , dan tidak tersedia untuk perusahaan , amal , kemitraan , atau entitas lain apa pun .",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(6, 126, 147, 1),
                        fontFamily: 'opensansSemiBold',
                        fontSize: 15),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  child: Text(
                    "7. Program ini terbuka untuk penduduk Indonesia yang berusia minimal 17 tahun.",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(6, 126, 147, 1),
                        fontFamily: 'opensansSemiBold',
                        fontSize: 15),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  child: Text(
                    "8. Vinyard Point berhak untuk menangguhkan atau mengakhiri Keanggotaan jika :",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(6, 126, 147, 1),
                        fontFamily: 'opensansSemiBold',
                        fontSize: 15),
                  ),
                ),
                Text(
                  "• Member melanggar Syarat & Ketentuan Vinyard Point;",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      color: Color.fromRGBO(6, 126, 147, 1),
                      fontFamily: 'opensansSemiBold',
                      fontSize: 15),
                ),
                Text(
                  "• Member belum menggunakan Aplikasi dalam waktu 24 bulan;",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      color: Color.fromRGBO(6, 126, 147, 1),
                      fontFamily: 'opensansSemiBold',
                      fontSize: 15),
                ),
                Text(
                  "• Member telah memberikan informasi pribadi yang tidak lengkap , tidak akurat , salah atau fiktif; dan",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      color: Color.fromRGBO(6, 126, 147, 1),
                      fontFamily: 'opensansSemiBold',
                      fontSize: 15),
                ),
                Text(
                  "• Member melakukan kecurangan atau penyalahgunaan Vinyard Point.",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      color: Color.fromRGBO(6, 126, 147, 1),
                      fontFamily: 'opensansSemiBold',
                      fontSize: 15),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  child: Text(
                    "9. Dengan berpartisipasi dalam Program , Member setuju untuk mengizinkan Vinyard Point berkomunikasi melalui surat , email , telepon , pesan teks , dan pemberitahuan melalui Aplikasi seluler.",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(6, 126, 147, 1),
                        fontFamily: 'opensansSemiBold',
                        fontSize: 15),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  child: Text(
                    "10. Vinyard Point dapat menggunakan saluran ini untuk mengomunikasikan status akun Member , memberi tahu Member ketika mereka memenuhi syarat untuk hadiah , mengomunikasikan perubahan program , menawarkan promosi khusus Member , informasi dan penawaran yang menurut Vinyard Point mungkin menarik bagi Member.",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(6, 126, 147, 1),
                        fontFamily: 'opensansSemiBold',
                        fontSize: 15),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  child: Text(
                    "11. Vinyard Point akan melindungi data Member sesuai dengan hukum yang berlaku di Indonesia.",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(6, 126, 147, 1),
                        fontFamily: 'opensansSemiBold',
                        fontSize: 15),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 25,bottom: 15),
                  child: Text(
                    "Poin Vinyard Point",
                    style: TextStyle(
                        color: Color.fromRGBO(6, 126, 147, 1),
                        fontFamily: 'opensansSemiBold',
                        fontSize: 15),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  child: Text(
                    "1.Member (anggota) berhak untuk mendapatkan Poin pada pembelian tertentu dan membelanjakan Poin untuk pembelian tertentu di masa mendatang di toko Vinyard.",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(6, 126, 147, 1),
                        fontFamily: 'opensansSemiBold',
                        fontSize: 15),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  child: Text(
                    "2.Member harus menginformasikan nomor member kepada kasir di Toko sebelum transaksi pembelian selesai untuk mendapatkan Poin.",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(6, 126, 147, 1),
                        fontFamily: 'opensansSemiBold',
                        fontSize: 15),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  child: Text(
                    "3.Member harus menunjukkan Kartu Digitalnya dalam Aplikasi Seluler Vinyard Point kepada kasir di Toko sebelum transaksi pembelian selesai untuk menghabiskan Poin.",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(6, 126, 147, 1),
                        fontFamily: 'opensansSemiBold',
                        fontSize: 15),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  child: Text(
                    "4. Member akan diberikan Poin yang jumlahnya harus ditentukan dari waktu ke waktu untuk setiap pembelian di Toko Vinyard . Poin diberikan berdasarkan harga bersih dari semua pajak dan biaya layanan.",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(6, 126, 147, 1),
                        fontFamily: 'opensansSemiBold',
                        fontSize: 15),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  child: Text(
                    "5. Poin akan diberikan ke akun maksimum akun dalam waktu 72 jam setelah pembelian.",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(6, 126, 147, 1),
                        fontFamily: 'opensansSemiBold',
                        fontSize: 15),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  child: Text(
                    "6. Poin tidak akan diperoleh saat membeli Voucher . Poin tidak bisa dihabiskan untuk membeli Voucher . ",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(6, 126, 147, 1),
                        fontFamily: 'opensansSemiBold',
                        fontSize: 15),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  child: Text(
                    "7. Poin tidak dapat ditebus dengan uang tunai , dan tidak dapat ditransfer .",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(6, 126, 147, 1),
                        fontFamily: 'opensansSemiBold',
                        fontSize: 15),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  child: Text(
                    "8. Poin milik lebik dari 1 Member tidak dapat digabungkan untuk membayar satu transaksi.",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(6, 126, 147, 1),
                        fontFamily: 'opensansSemiBold',
                        fontSize: 15),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  child: Text(
                    "9. Ketika seorang member menebus Poinnya, Poin tersebut akan langsung dikurangkan dari akun Member.",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(6, 126, 147, 1),
                        fontFamily: 'opensansSemiBold',
                        fontSize: 15),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  child: Text(
                    "10. Jika Vinyard Point salah memotong Poin Member, Vinyard Point bertanggung jawab atas pengembalian Poin ke akun Member",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(6, 126, 147, 1),
                        fontFamily: 'opensansSemiBold',
                        fontSize: 15),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  child: Text(
                    "11. Jika Vinyard Point membatalkan Membership atau menghentikan Program karena alasan apa pun, semua Poin yang diperoleh pada akun Member akan hangus.",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(6, 126, 147, 1),
                        fontFamily: 'opensansSemiBold',
                        fontSize: 15),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  child: Text(
                    "12. Tanggal kedaluwarsa Poin Vinyard Point mengikuti kalender tahunan, semua Poin Vinyard Point akan berakhir pada tanggal 31 Desember untuk tahun yang bersangkutan.",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Color.fromRGBO(6, 126, 147, 1),
                        fontFamily: 'opensansSemiBold',
                        fontSize: 15),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
