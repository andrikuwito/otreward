import 'dart:convert';
import 'package:another_flushbar/flushbar.dart';
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:vinyard_point/page/mobileBody.dart';
import 'package:vinyard_point/preferences.dart';
import 'package:vinyard_point/signin/forgot.dart';
import 'package:vinyard_point/signin/signup.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class SignIn extends StatefulWidget {
  SignIn({Key? key}) : super(key: key);

  @override
  State<SignIn> createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final telp = TextEditingController();
  final pass = TextEditingController();
  List user = [];
  Future<String> login(String notelp, String password) async {
    var bytes = utf8.encode(password);
    String digest = sha1.convert(bytes).toString();
    try {
      var response = await http.post(
          Uri.parse("http://116.213.55.69/LSPoin/api/LSPoin/SignIn"),
          headers: {"Content-Type": "application/json; charset=utf-8"},
          body: json.encode({"ID": notelp, "Pass": digest, "Company": "VY"}));
      if (response.statusCode == 200) {
        if (json.decode(response.body)['StatusCode'] == 1) {
          user.add(json.decode(response.body)['ReturnObject']);
          print("sukses");
          return "sukses";
        } else {
          print("SALAH");
          return "gagal";
        }
      } else {
        return "gagal";
      }
    } catch (e) {
      print(e.toString());
      return "gagal";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Center(
        child: Container(
          width: double.infinity,
          decoration: BoxDecoration(
              image: DecorationImage(
            image: AssetImage("assets/images/background.jpeg"),
            fit: BoxFit.cover,
          )),
          padding: EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Image.asset("assets/images/logo.png"),
              Text(
                "SELAMAT DATANG",
                style: TextStyle(
                    fontFamily: 'opensansExtraBold',
                    fontSize: 18,
                    fontWeight: FontWeight.bold),
              ),
              Form(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Container(
                    padding: EdgeInsets.only(
                      left: 30,
                      right: 30,
                    ),
                    child: Column(
                      children: [
                        TextField(
                          decoration: InputDecoration(
                              labelText: 'Email or Phone',
                              labelStyle: TextStyle(
                                  color: Color.fromRGBO(6, 126, 147, 1)),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25),
                                borderSide: BorderSide(
                                    width: 1,
                                    color: Color.fromRGBO(6, 126, 147, 1)),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: const BorderSide(
                                    width: 1,
                                    color: Color.fromRGBO(6, 126, 147, 1)),
                                borderRadius: BorderRadius.circular(25),
                              )),
                          controller: telp,
                        ),
                        Container(
                          padding: EdgeInsets.only(bottom: 20),
                        ),
                        TextField(
                          decoration: InputDecoration(
                              labelText: 'Password',
                              labelStyle: TextStyle(
                                  color: Color.fromRGBO(6, 126, 147, 1)),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25),
                                borderSide: BorderSide(
                                    width: 1,
                                    color: Color.fromRGBO(
                                        6, 126, 147, 1)), //<-- SEE HERE
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: const BorderSide(
                                    width: 1,
                                    color: Color.fromRGBO(6, 126, 147, 1)),
                                borderRadius: BorderRadius.circular(25),
                              )),
                          controller: pass,
                          obscureText: true,
                          enableSuggestions: false,
                          autocorrect: false,
                        ),
                      ],
                    ),
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        padding: EdgeInsets.zero,
                        primary: Colors.transparent,
                        shadowColor: Colors.transparent,
                        elevation: 0.0),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ForgotPassword()));
                    },
                    child: Text(
                      "Lupa Kata Sandi?",
                      style: TextStyle(
                          fontFamily: 'opensansSemiBold', color: Colors.red),
                    ),
                  ),
                ],
              )),
              Container(
                width: MediaQuery.of(context).size.width * 0.8,
                height: MediaQuery.of(context).size.height * 0.065,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0),
                      ),
                      padding: EdgeInsets.zero,
                      primary: Color.fromRGBO(6, 126, 147, 1),
                      shadowColor: Colors.transparent,
                      elevation: 0.0),
                  onPressed: () {
                    if (telp.text == "") {
                      Flushbar(
                        flushbarPosition: FlushbarPosition.TOP,
                        backgroundColor: Colors.red,
                        message: "Email / Telp tidak boleh kosong",
                        duration: Duration(seconds: 3),
                      )..show(context);
                    } else {
                      if (pass.text == "") {
                        Flushbar(
                          flushbarPosition: FlushbarPosition.TOP,
                          backgroundColor: Colors.red,
                          message: "Password tidak boleh kosong",
                          duration: Duration(seconds: 3),
                        )..show(context);
                      } else {
                        login(telp.text, pass.text).then((value) {
                          if (value == "sukses") {
                            initializePreference(
                                true,
                                user[0]["Email"],
                                user[0]["Phone"],
                                user[0]["CustomerName"],
                                user[0]["AccountNo"],
                                user[0]["CustomerID"]);
                            Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(
                                    builder: (context) => MyMobile()),
                                (Route<dynamic> route) => false);
                          } else if (value == "gagal") {
                            Flushbar(
                              flushbarPosition: FlushbarPosition.TOP,
                              backgroundColor: Colors.red,
                              message:
                                  "User tidak ditemukan atau password salah",
                              duration: Duration(seconds: 3),
                            )..show(context);
                          }
                        });
                      }
                    }
                  },
                  child: Text("MASUK"),
                ),
              ),
              Container(
                child: Column(
                  children: [
                    Text(
                      "Tidak Memiliki Akun?",
                      style: TextStyle(color: Colors.white),
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          padding: EdgeInsets.zero,
                          primary: Colors.transparent,
                          shadowColor: Colors.transparent,
                          elevation: 0.0),
                      onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => SignUp()));
                      },
                      child: Text(
                        "Klik Disini",
                        style: TextStyle(color: Colors.red, fontSize: 16),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
