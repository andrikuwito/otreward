import 'dart:convert';

import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:http/http.dart' as http;
import 'package:vinyard_point/signin/signin.dart';

class ForgotPassword extends StatefulWidget {
  const ForgotPassword({Key? key}) : super(key: key);

  @override
  State<ForgotPassword> createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  var email = TextEditingController();
  Future<String> forgotpassword(String email) async {
    var response = await http.post(
        Uri.parse("http://116.213.55.69/LSPoin/api/LSPoin/ForgotPassword"),
        headers: {"Content-Type": "application/json; charset=utf-8"},
        body: json.encode({"EmailOrPassword": email, "Company": "VY"}));
    if (response.statusCode == 200) {
      print(json.decode(response.body)['StatusCode']);
      print(json.decode(response.body)['ReturnObject']['Message']);
      if (json.decode(response.body)['StatusCode'] == 1) {
        if (json.decode(response.body)['ReturnObject']['Message'] ==
            "Anda telah mengajukan lupa password lebih dari 3 kali, harap periksa email anda.") {
          return "Anda telah mengajukan lupa password lebih dari 3 kali, harap periksa email anda.";
        } else {
          return "sukses";
        }
      } else {
        if (json.decode(response.body)['ReturnObject']['Message'] ==
            "User tidak ditemukan") {
          return "User tidak ditemukan";
        } else {
          return "gagal";
        }
      }
    } else {
      return "gagal";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
          leading: CloseButton(
            color: Colors.black,
          ),
          elevation: 0,
          backgroundColor: Colors.transparent),
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage("assets/images/background.jpeg"),
          fit: BoxFit.cover,
        )),
        child: Container(
            padding: EdgeInsets.all(30),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    Text("LUPA KATA SANDI",
                        style: TextStyle(
                            fontFamily: 'opensansExtraBold',
                            fontSize: 18,
                            fontWeight: FontWeight.bold)),
                    Container(
                      padding: EdgeInsets.only(bottom: 30),
                    ),
                    Container(
                      padding: EdgeInsets.only(right: 20, left: 20),
                      child: TextField(
                        decoration: InputDecoration(
                            labelText: 'Alamat Email / Nomor HP',
                            labelStyle: TextStyle(
                                color: Color.fromRGBO(6, 126, 147, 1)),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(25),
                              borderSide: BorderSide(
                                  width: 2,
                                  color: Color.fromRGBO(
                                      6, 126, 147, 1)), //<-- SEE HERE
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  width: 2,
                                  color: Color.fromRGBO(6, 126, 147, 1)),
                              borderRadius: BorderRadius.circular(25),
                            )),
                        controller: email,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(bottom: 30),
                    ),
                    Text(
                      "Tidak masalah! Cukup berikan ID Email atau No. HP Anda dan kami akan mengirim password baru anda",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontFamily: 'opensansReguler',
                          fontSize: 15,
                          fontWeight: FontWeight.w500),
                    ),
                  ],
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.7,
                  height: MediaQuery.of(context).size.height * 0.065,
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(30.0),
                          ),
                          padding: EdgeInsets.zero,
                          primary: Color.fromRGBO(6, 126, 147, 1),
                          shadowColor: Colors.transparent,
                          elevation: 0.0),
                      onPressed: () async {
                        if (email.text == "") {
                          Flushbar(
                            flushbarPosition: FlushbarPosition.TOP,
                            backgroundColor: Colors.red,
                            message: "Email / Telp tidak boleh kosong",
                            duration: Duration(seconds: 3),
                          )..show(context);
                        } else {
                          showDialog(
                              barrierDismissible: false,
                              context: context,
                              builder: (_) {
                                return Center(
                                  child: SizedBox(
                                      width: 50,
                                      height: 50,
                                      child: CircularProgressIndicator()),
                                );
                              });
                          await forgotpassword(email.text).then((value) {
                            if (value == "sukses") {
                              Navigator.of(context).pop();
                              Navigator.of(context).pushAndRemoveUntil(
                                  MaterialPageRoute(
                                      builder: (context) => SignIn()),
                                  (Route<dynamic> route) => false);
                              Flushbar(
                                flushbarPosition: FlushbarPosition.TOP,
                                backgroundColor: Colors.green,
                                message: "Berhasil Ganti Password",
                                duration: Duration(seconds: 3),
                              )..show(context);
                            } else if (value ==
                                "Anda telah mengajukan lupa password lebih dari 3 kali, harap periksa email anda.") {
                              Navigator.of(context).pop();
                              Flushbar(
                                flushbarPosition: FlushbarPosition.TOP,
                                backgroundColor: Colors.green,
                                message:
                                    "Anda telah mengajukan lupa password lebih dari 3 kali, harap periksa email anda.",
                                duration: Duration(seconds: 3),
                              )..show(context);
                              Navigator.of(context).pushAndRemoveUntil(
                                  MaterialPageRoute(
                                      builder: (context) => SignIn()),
                                  (Route<dynamic> route) => false);
                            } else if (value == "User tidak ditemukan") {
                              Navigator.of(context).pop();
                              Flushbar(
                                flushbarPosition: FlushbarPosition.TOP,
                                backgroundColor: Colors.red,
                                message: "User tidak ditemukan",
                                duration: Duration(seconds: 3),
                              )..show(context);
                            } else {
                              Navigator.of(context).pop();
                              Flushbar(
                                flushbarPosition: FlushbarPosition.TOP,
                                backgroundColor: Colors.green,
                                message: "Gagal",
                                duration: Duration(seconds: 3),
                              )..show(context);
                            }
                          });
                        }
                      },
                      child: Text("RESET PASSWORD")),
                )
              ],
            )),
      ),
    );
  }
}
