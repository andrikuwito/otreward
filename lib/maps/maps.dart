import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'locations.dart' as locations;
import 'dart:ui' as ui;

class Maps extends StatefulWidget {
  const Maps({Key? key}) : super(key: key);

  @override
  State<Maps> createState() => _MapsState();
}

class _MapsState extends State<Maps> {
  late GoogleMapController mapController;
  final Map<String, Marker> _markers = {};
  List<dynamic> _data = [];

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))!
        .buffer
        .asUint8List();
  }

  Future<void> _onMapCreated(GoogleMapController controller) async {
    final Uint8List markerbitmap =
        await getBytesFromAsset('assets/images/pointer.png', 100);
    final googleOffices = await locations.getGoogleOffices();
    for (final office in googleOffices.offices) {
      if (office.company == "VY") {
        print("wdawa");
        final marker = Marker(
          markerId: MarkerId(office.name),
          position: LatLng(office.lat, office.lng),
          infoWindow: InfoWindow(
            title: office.name,
          ),
          icon: BitmapDescriptor.fromBytes(markerbitmap),
        );
        var desc = [
          {
            "id": office.id,
            "name": office.name,
            "alamat": office.address,
            "position": LatLng(office.lat, office.lng)
          }
        ];
        setState(() {
          _markers[office.name] = marker;
          if (office.company == "VY") {
            _data.addAll(desc);
          }
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    GoogleMap(
      onMapCreated: (controller) {
        FutureBuilder(
            future: _onMapCreated(controller),
            initialData: [],
            builder: (builder, snapshot) {
              if (snapshot.connectionState ==
                  ConnectionState
                      .waiting) //While waiting for response return this
                return Center(
                    child: Container(child: CircularProgressIndicator()));
              return getBodyMaps();
            });
        setState(() {
          mapController = controller;
        });
      },
      initialCameraPosition: const CameraPosition(
        target: LatLng(-6.155301876746913, 106.90821313469817),
        zoom: 20,
      ),
      markers: _markers.values.toSet(),
    );

    return Scaffold(
        appBar: AppBar(
          title: const Text('Lokasi Toko Vinyard'),
          backgroundColor: Color.fromRGBO(13, 125, 160, 1),
        ),
        body: getBodyMaps());
  }

  Widget getBodyMaps() {
    return Container(
      child: Stack(
        children: [
          GoogleMap(
            onMapCreated: (controller) {
              _onMapCreated(controller);
              setState(() {
                mapController = controller;
              });
            },
            initialCameraPosition: const CameraPosition(
              target: LatLng(-6.155301876746913, 106.90821313469817),
              zoom: 20,
            ),
            markers: _markers.values.toSet(),
          ),
          Container(
              alignment: Alignment.bottomCenter,
              padding: EdgeInsets.only(left: 10, right: 10, bottom: 100),
              child: SizedBox(
                height: MediaQuery.of(context).size.height * 0.17,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: _data.length,
                    itemBuilder: (BuildContext context, int index) {
                      return ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.zero,
                            primary: Colors.transparent,
                            elevation: 0.0),
                        onPressed: () {
                          LatLng newlatlang = _data[index]["position"];
                          mapController.animateCamera(
                              CameraUpdate.newCameraPosition(CameraPosition(
                                  target: newlatlang, zoom: 17)));
                        },
                        child: Card(
                          child: Container(
                            padding: EdgeInsets.all(10),
                            width: MediaQuery.of(context).size.width * 0.55,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  _data[index]["id"],
                                  style: TextStyle(
                                      fontSize: 10,
                                      color: Color.fromRGBO(6, 126, 147, 1)),
                                ),
                                Text(_data[index]["name"],
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold)),
                                Text(_data[index]["alamat"],
                                    style: TextStyle(fontSize: 12))
                              ],
                            ),
                          ),
                        ),
                      );
                    }),
              ))
        ],
      ),
    );
  }
}
